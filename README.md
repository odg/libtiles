# libtiles

This is a game engine for crossword based board games, inspired by Scrabble and
its [many variants](https://en.wikipedia.org/wiki/Scrabble_variants). It is
designed to be used as a backend for word game apps.

# Use

The library uses the Meson build system, and is usable as a Meson
[subproject](https://mesonbuild.com/Subprojects.html). To do so, add this to
your `meson.build` file:

    libtiles_dep = dependency('libtiles', fallback: ['libtiles', 'tiles_dep'])

Read the [header file](include/tiles.h) for documentation of the libtiles API.

To use, the only dependency is [glib](https://gitlab.gnome.org/GNOME/glib).

# Building

To build, you will need [Meson](https://mesonbuild.com/), Ninja, a C compiler,
and the glib header files.

To get the prerequisites on Debian/Ubuntu:

    sudo apt install build-essential meson libglib2.0-dev

Then, you can build:

    meson build
    cd build
    ninja
    sudo ninja install

# License

This library is free software released under the terms of the GNU General
Public License, version 3 or later.
