# Scrabble Variants

Notes about variants and languages of Scrabble. Information here is collected
from Wikipedia, Scrabble3D and Quackle.

## Variants with standard boards and local names

Local variants with a unique localised name, standard board, but different
alphabet. These local names should be recognised by the engine as the standard
board, and default to their matching alphabet language.

* Afrikaans
 * Krabbel
* Breton
 * Skrabell
* Faroese
 * Krossorðaspæl
* Hebrew
 * נא‎-שבץ‎
* Italian
 * Scarabeo (old)
* Māori
 * Scramble
 * Kuputupu
* Pinyin
 * PinyinPal
* Spanish
 * Edición en Español (North America)
 * Escarbar (Latin America)
* Swedish
 * Alfapet/Alfa-pet (pre-1990)

## Other variants with standard boards

These variants have no special names but an alphabet for the language is
available, which is for use with the standard board. Therefore the language
needs to be specified for these.

## Variants with non-standard boards

These named variants have a unique, non-standard board, in addition to their
own alphabet. Some variants are available in multiple languages, so you can't
assume the language from the variant and vice versa.

* Armenian
 * ԲԱՌ ԽԱՂ (17x17)
* Catalan
 * Super (21x21)
* English
 * Super (21x21)
 * Literaxx (Literaki board - 15x15)
* German
 * Super (21x21)
* Igbo
 * ? (19x19)
* Italian
 * Scarabeo (17x17)
* Polish
 * Literaki (15x15)
 * Scriba (new Alfapet board - 17x17)
* Swedish
 * Alfapet (1990 onwards) (17x17)

## Other translations

These are just translations on the word Scrabble, from Wikipedia.

* ar سكرابل
* bg Скрабъл
* br Skrabell
* el Σκραμπλ
* eo Skrablo
* fa اسکربل
* gu સ્ક્રેબલ
* he שבץ נא
* hi स्क्रैबल
* ja スクラブル
* kn ಸ್ಕ್ರ್ಯಾಬಲ್‌
* ko 스크래블
* la Scrabularum ludus
* lt Kriss-Kross
* ml സ്ക്രാബിൾ
* ru Скрэббл
* sh Kvizovka
* sr Скрабл
* ta சொல்லாக்க ஆட்டம்
* te స్క్రాబుల్
* th สแคร็บเบิล
* tyv Сөстээрек
* uk Скрабл
