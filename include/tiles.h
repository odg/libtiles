/*
 * Single public header file for libtiles.
 * libtiles is the backend for creating and playing crossword-based board games
 * such as Scrabble.
 *
 * Functions that return pointers return NULL on error.
 * Functions that return numerical types return a negative value on error.
 *
 * All input and output strings/files are UTF-8 encoded.
 * All IDs and indices are zero based.
 */

#ifndef _TILES_H_
#define _TILES_H_

#include <glib.h>
#include <gmodule.h>

G_BEGIN_DECLS

/* GError domains and error codes. */
#include <tiles_err.h>

/* Function attributes. */
#ifndef __has_attribute
# define __has_attribute(x) 0
#endif
#ifndef __has_declspec_attribute
# define __has_declspec_attribute(x) 0
#endif

#if __has_attribute(nonnull)
# define NONNULL __attribute__ ((nonnull))
#else
# define NONNULL
#endif

/** External function API linkage. */
#ifndef TILES_API
# if defined(TILES_BUILD_SHARED)
#  define TILES_API G_MODULE_EXPORT
# elif (defined(G_OS_WIN32) || __has_declspec_attribute(dllimport)) && !defined(TILES_BUILD_STATIC)
#  define __declspec(dllimport)
# else
#  define TILES_API
# endif
#endif

/**
 * Function return codes. If < 0, represents an error, otherwise represents the game's state.
 */
typedef enum tiles_code {
	TILES_OK        = 0,
	TILES_GAME_OVER = 1,
	TILES_ERROR     = -1
} tiles_code;

/**
 * The four types of move.
 */
typedef enum move_type {
	MOVE_TYPE_NORMAL = 0,
	MOVE_TYPE_CHANGE = 1,
	MOVE_TYPE_PASS   = 2,
	MOVE_TYPE_RESIGN = 3
} move_type;

/**
 * Direction of a word on a board. The values represent the matching emoji.
 */
typedef enum dir_t {
	DIR_UP    = 0x2B06,
	DIR_DOWN  = 0x2B07,
	DIR_LEFT  = 0x2B05,
	DIR_RIGHT = 0x27A1
} dir_t;

/**
 * A type represention a vector - position and a direction on the board.
 */
typedef struct pos_t
{
	guint row;
	guint col;
	dir_t dir;
} pos_t;

/** Game objects **/
typedef struct game_t game_t;

/** Freeing memory **/

/**
 * Free all memory associated with the game's context. The game object cannot be used again after this.
 * @game: the game object to free
 */
TILES_API void
tiles_game_free(game_t *game);

/**
 * A generic (on C11 compilers only) convenience macro which frees then NULLs, to avoid double frees.
 */
#if __STDC_VERSION__ >= 201112L
#define tiles_free(x) \
    g_clear_pointer(&(x),                               \
                    _Generic((x),                       \
                             game_t *: tiles_game_free, \
                              char **: g_strfreev,      \
                              default: g_free           \
                            ))
#else
#define tiles_free(x) g_clear_pointer(&(x), tiles_game_free)
#endif

// Enable using g_autoptr with game_t
G_DEFINE_AUTOPTR_CLEANUP_FUNC(game_t, tiles_game_free)

/**
 * SECTION:playing
 * @short_description: High level functions to create and play a game
 * @title: Playing The Game
 */

/**
 * tiles_game_new:
 * @lang: the language to load
 * @mode: the name of the mode/board to load
 * @data_dir: the directory to load data files from, or NULL
 * @err: return location for a GError, or NULL
 *
 * Allocate and initialise a new game structure, and try to load the board, lexicon and alphabet.
 * If something goes wrong, an error message is printed, all memory is freed, and NULL is returned.
 * The resulting pointer represents a game's state, and should be freed with destroy_game().
 *
 * Returns: a newly allocated game object, or NULL on error
 */
TILES_API game_t *
tiles_game_new(const char  *lang,
               const char  *mode,
               const char  *data_dir,
               GError     **err)
G_GNUC_WARN_UNUSED_RESULT;

/**
 * tiles_game_start:
 * @game: the game object
 * @players: number of players
 * @err: return location for a GError, or NULL
 *
 * Start a game with the given number of players. Resets the board and remaining letters, and
 * refills the racks. Must be called after creating a new game, before making a move. Calling this
 * during a game will do nothing. This allows repeated games to be played using the same structure,
 * avoiding needing to reload the files.
 *
 * Returns: a tiles_code value
 */
TILES_API tiles_code
tiles_game_start(game_t  *game,
                 guint    players,
                 GError **err)
G_GNUC_WARN_UNUSED_RESULT;

/**
 * tiles_move_new:
 * @game: the game object
 * @word: the list of tile indices
 * @word_len: the number of tiles to place
 * @start: the position of the start of the word
 * @move_type: the type of move
 * @err: return location for a GError, or NULL
 *
 * Make a move. The move is assigned to whichever player is due to play next.
 * The type determines the kind of move, and can be one of four types. If changing, the word is
 * used as the list of letters to change. If passing or resigning, the word is ignored.
 *
 * Returns: a tiles_code value
 */
TILES_API tiles_code
tiles_move_new(game_t      *game,
               const gint  *word,
               gsize        word_len,
               pos_t       *start,
               move_type    type,
               GError     **err)
G_GNUC_WARN_UNUSED_RESULT;

TILES_API tiles_code
tiles_move_new_from_ref(game_t      *game,
                        const gint  *word,
                        gsize        word_len,
                        const char  *ref,
                        GError     **err)
G_GNUC_WARN_UNUSED_RESULT;

TILES_API tiles_code
tiles_move_new_from_str(game_t      *game,
                        const char  *word_str,
                        const char  *ref,
                        GError     **err)
G_GNUC_WARN_UNUSED_RESULT;

#define TILES_CHANGE(game, word, word_len) tiles_move_new((game), (word), (word_len), NULL, MOVE_TYPE_CHANGE, NULL)
#define TILES_PASS(game)                   tiles_move_new((game), NULL,   0,          NULL, MOVE_TYPE_PASS,   NULL)
#define TILES_RESIGN(game)                 tiles_move_new((game), NULL,   0,          NULL, MOVE_TYPE_RESIGN, NULL)

/**
 * SECTION:saving
 * @short_description: Saving and loading a game from file
 * @title: Saving The Game
 */

/**
 * Save a game to a file at the given path.
 */
TILES_API int
tiles_game_save(const game_t *game,
                const char   *path);

/**
 * Load a saved game from a file at the given path.
 */
TILES_API game_t *
tiles_game_load(const char *path)
G_GNUC_WARN_UNUSED_RESULT;

/**
 * SECTION:query
 * @short_description: Querying and retrieving data from the game objects
 * @title: Querying The Game
 */

/**
 * Get the duration of the game, in microseconds
 */
TILES_API GTimeSpan
tiles_get_duration(const game_t *game)
NONNULL;

/**
 * Get the unix timestamp of when the game started, or 0 if it hasn't started
 */
TILES_API gint64
tiles_get_start_time(const game_t *game)
NONNULL;

/**
 * Get the unix timestamp of when the game ended, or 0 if it's still in progress
 */
TILES_API gint64
tiles_get_end_time(const game_t *game)
NONNULL;

/**
 * How many moves have been played in the game so far?
 */
TILES_API guint
tiles_get_num_moves(const game_t *game)
G_GNUC_PURE NONNULL;

/**
 * How many players are participating in the game?
 */
TILES_API guint
tiles_get_num_players(const game_t *game)
G_GNUC_PURE NONNULL;

/**
 * What is the ID (zero based) of the player whose move is next?
 */
TILES_API guint
tiles_get_next_player(const game_t *game)
G_GNUC_PURE NONNULL;

/**
 * What is the width and height, in squares, of the board?
 */
TILES_API guint
tiles_get_board_size(const game_t *game)
G_GNUC_PURE NONNULL;

/**
 * How many tiles are in a rack?
 */
TILES_API guint
tiles_get_rack_size(const game_t *game)
G_GNUC_PURE NONNULL;

/**
 * How many tiles are left in the bag?
 */
TILES_API gulong
tiles_get_remaining(const game_t *game)
G_GNUC_PURE NONNULL;

/**
 * How many points does a player get for using all their tiles in one move?
 */
TILES_API int
tiles_get_bonus(const game_t *game)
G_GNUC_PURE NONNULL;

/**
 * Get the name of the word list's language currently loaded
 */
TILES_API const char *
tiles_get_lang(const game_t *game)
G_GNUC_PURE NONNULL;

/**
 * Get the name of the board mode in use (e.g. Scrabble, Words with Friends, etc.)
 */
TILES_API const char *
tiles_get_mode(const game_t *game)
G_GNUC_PURE NONNULL;

/**
 * Get the word of the given move ID
 */
TILES_API const char *
tiles_get_move_word(const game_t  *game,
                    guint          move,
                    GError       **err);

/**
 * Get the score of the given move ID
 */
TILES_API long
tiles_get_move_score(const game_t  *game,
                     guint          move,
                     GError       **err);

/**
 * Get the tiles in the rack of the given player ID
 */
TILES_API const gint *
tiles_get_player_rack(const game_t  *game,
                      guint          player,
                      GError       **err);

/**
 * Get the score of the given player ID
 */
TILES_API long
tiles_get_player_score(const game_t  *game,
                       guint          player,
                       GError       **err);

/**
 * Check if a word is valid in the currently loaded lexicon
 */
TILES_API gboolean
tiles_is_word_valid(const game_t *game,
                    const char   *word)
G_GNUC_PURE NONNULL;

TILES_API pos_t *
tiles_parse_square_ref(const game_t *game,
                       const char   *ref,
                       pos_t        *pos);

TILES_API char **
tiles_get_square_label_list(const game_t *game);

TILES_API long
tiles_get_letter_score(const game_t *game, guint letter, GError **err);

/**
 * Check the scores to find the winner.
 * Returns the number of winners. If 0, game hasn't started. If > 1, there is a tie.
 */
TILES_API gsize
tiles_check_winners(const game_t *game, glong **scores_in, guint **winners, GError **err);


/**
 * SECTION:output
 * @short_description: Generated formatted output suitable for display
 * @title: Displaying The Game
 */

/**
 * Generate a string representing the game's duration so far, in the format HH:MM:SS. Should be freed after use.
 */
TILES_API char *
tiles_get_duration_str(const game_t *game,
                       int           dp)
G_GNUC_WARN_UNUSED_RESULT;

/**
 * Generate a string representing the game's duration so far, in a more verbose format. Should be freed after use.
 */
TILES_API char *
tiles_get_duration_str_long(const game_t *game,
                            int           dp)
G_GNUC_WARN_UNUSED_RESULT;

/**
 * Get a string representing the given move ID.
 */
TILES_API char *
tiles_get_move_str(const game_t  *game,
                   guint          move,
                   GError       **err)
G_GNUC_WARN_UNUSED_RESULT;

/**
 * Get the nth letter of the loaded alphabet, as a UTF-8 string.
 */
TILES_API const char *
tiles_get_letter(const game_t *game,
                 guint         n)
G_GNUC_PURE;

/**
 * Get a string representing the square at the given row and column.
 */
TILES_API char *
tiles_get_square_ref(const game_t *game,
                     pos_t         pos)
G_GNUC_WARN_UNUSED_RESULT G_GNUC_PURE;

/**
 * Get the label of the given square's type, if it is of a premium type.
 */
TILES_API const char *
tiles_get_square_label(const game_t *game,
                       pos_t         pos)
G_GNUC_PURE;

/**
 * Get the long version of the label of the given square's type, if it is of a premium type.
 */
TILES_API const char *
tiles_get_square_label_long(const game_t *game,
                            pos_t         pos)
G_GNUC_PURE;

/**
 * Get the character of the tile on the given square, if there is one.
 */
TILES_API gint
tiles_get_square_tile(const game_t *game,
                      pos_t         pos,
                      gboolean     *blank)
G_GNUC_PURE;

/**
 * Get the character of the tile on the given square, if there is one, as a UTF-8 string.
 */
TILES_API const char *
tiles_get_square_tile_label(const game_t *game,
                            pos_t         pos,
                            gboolean     *blank)
G_GNUC_PURE;

G_END_DECLS
#endif // _TILES_H_
