/* GError domains and codes for libtiles. */

#ifndef _TILES_ERR_H_
#define _TILES_ERR_H_

#include <glib.h>

G_BEGIN_DECLS

#define TILES_MOVE_ERROR tiles_move_error_quark()
#define TILES_FILE_ERROR tiles_file_error_quark()
#define TILES_ARGS_ERROR tiles_args_error_quark()

typedef enum {
/* When validating a move. */
	TILES_MOVE_ERROR_DIRECTION,
	TILES_MOVE_ERROR_OUTOFBOUNDS,
	TILES_MOVE_ERROR_MISSINGTILE,
	TILES_MOVE_ERROR_BADLETTER,
	TILES_MOVE_ERROR_BADSIDEWORD,
	TILES_MOVE_ERROR_BADMAINWORD,
	TILES_MOVE_ERROR_BADSTART,
	TILES_MOVE_ERROR_NOTOUCH,
	TILES_MOVE_ERROR_TOOSHORT,
	TILES_MOVE_ERROR_TOOFEWTILES,
/* When adding a new move. */
	TILES_MOVE_ERROR_PREGAME,
	TILES_MOVE_ERROR_POSTGAME,
	TILES_MOVE_ERROR_NOWORD,
	TILES_MOVE_ERROR_BADMOVETYPE,
	TILES_MOVE_ERROR_BADPOSREF,
} TilesMoveError;

typedef enum {
/* When loading files. */
	TILES_FILE_ERROR_NOFILE,
	TILES_FILE_ERROR_HASHTAB,
	TILES_FILE_ERROR_BADTYPE,
	TILES_FILE_ERROR_BADWIDTH,
	TILES_FILE_ERROR_MISSINGROWS,
	TILES_FILE_ERROR_MISSINGCOLS,
} TilesFileError;

typedef enum {
/* When querying the game. */
	TILES_ARGS_ERROR_BADMOVEID,
	TILES_ARGS_ERROR_BADPLAYERID,
/* When initialising the game. */
	TILES_ARGS_ERROR_NOLANG,
	TILES_ARGS_ERROR_NOMODE,
	TILES_ARGS_ERROR_LOWPLAYERS,
	TILES_ARGS_ERROR_INPROGRESS,
} TilesArgsError;

GQuark tiles_move_error_quark(void);
GQuark tiles_file_error_quark(void);
GQuark tiles_args_error_quark(void);

G_END_DECLS

#endif // _TILES_ERR_H_
