#include "files.h"
#include "setup.h"

static inline char *
build_path(const char *root, const char *subdir, const char *filename)
{
	if(subdir) {
		return g_build_filename(root, subdir, filename, NULL);
	} else {
		return g_build_filename(root, filename, NULL);
	}
}

static inline char *
get_raw_data(const char *datadir, const char *subdir, const char *filename, GError **err)
{
	gboolean success;
	gchar *res = NULL;
	gchar *path;
	if(datadir) {
		path = build_path(datadir, subdir, filename);
	} else {
		const gchar *user_datadir = g_get_user_data_dir();
		path = build_path(user_datadir, subdir, filename);
	}
	success = g_file_get_contents(path, &res, NULL, NULL);
	g_free(path);
	if(!success) {
		const gchar * const * system_datadirs = g_get_system_data_dirs();
		gsize i = 0;
		while(system_datadirs[i]) {
			path = build_path(system_datadirs[i++], subdir, filename);
			success = g_file_get_contents(path, &res, NULL, NULL);
			g_free(path);
			if(success) {
				break;
			}
		}
	}
	if(!res) {
		g_set_error(err, TILES_FILE_ERROR, TILES_FILE_ERROR_NOFILE, _("could not find or open file '%s'"), filename);
	}
	return res;
}

/*
 * Load lexicon (word list) into a structure.
 * Lexica should be UTF-8 encoded, with words seperated by whitespace (usually per line).
 * Make sure each word is: unique, uppercase, nonempty, and stripped.
 */
GHashTable *
load_lexicon(const gchar  *lang,
             const gchar  *lang_var,
             const gchar  *datadir,
             GError      **err)
{
	gchar *filename, *raw, *ptr;
	GHashTable *res;
	res = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);
	if(!res) {
		g_set_error(err, TILES_FILE_ERROR, TILES_FILE_ERROR_HASHTAB, _("could not create hash table"));
		return NULL;
	}

	if(lang_var) {
		filename = g_strconcat(lang_var, "_", lang, LEXICA_EXT, NULL);
	} else {
		filename = g_strconcat(lang, LEXICA_EXT, NULL);
	}
	raw = get_raw_data(datadir, LEXICA_DIR, filename, err);
	g_free(filename);
	if(!raw) {
		g_hash_table_destroy(res);
		return NULL;
	}
	ptr = raw;
	while(*ptr) {
		gchar *start = ptr;
		while(g_unichar_isgraph((gunichar)(*ptr))) {
			ptr = g_utf8_next_char(ptr);
		}
		while(g_unichar_isspace((gunichar)(*ptr))) {
			*ptr = 0;
			ptr = g_utf8_next_char(ptr);
		}
		if(*start) {
			g_hash_table_add(res, g_utf8_strup(start, -1));
		}
	}
	g_free(raw);
	return res;
}

/*
 * Load the boards.ini file, which contains formal descriptions of various boards/modes.
 * The file format is described in the file's comments.
 */
square_t **
load_board(game_t       *game,
           const gchar  *datadir,
           GError      **err)
{
	int tmp;
	g_autoptr(GError) tmp_err = NULL;
	g_autoptr(GKeyFile) key_file = g_key_file_new();
	g_key_file_set_list_separator(key_file, ',');

	if(datadir) {
		const gchar *search_dirs[2] = {datadir, NULL};
		if(!g_key_file_load_from_dirs(key_file, "boards.ini", search_dirs, NULL, G_KEY_FILE_NONE, err)) {
			return NULL;
		}
	} else {
		if(!g_key_file_load_from_data_dirs(key_file, "boards.ini", NULL, G_KEY_FILE_NONE, err)) {
			return NULL;
		}
	}

	game->square_types = g_array_new(FALSE, TRUE, sizeof(premium_t));
	gsize i = 1;
	char key[8];
	while(1) {
		premium_t new_type;
		gsize parts_len;
		g_snprintf(key, sizeof(key), "type_%lu", i);
		gchar **parts = g_key_file_get_string_list(key_file, game->mode, key, &parts_len, &tmp_err);
		if(!parts || tmp_err) {
			g_clear_error(&tmp_err);
			break;
		}
		if(parts_len != 3) {
			g_set_error(err, TILES_FILE_ERROR, TILES_FILE_ERROR_BADTYPE, _("invalid premium square type (%lu parts found in %s)"), parts_len, key);
			g_strfreev(parts);
			return NULL;
		}
		new_type.label = g_strdup(parts[0]);
		new_type.multi = strtol(parts[1], NULL, 0);
		new_type.word  = (parts[2][0] == 'W' || parts[2][0] == 'w') ? TRUE : FALSE;
		g_array_append_val(game->square_types, new_type);
		g_strfreev(parts);
		i++;
	}
	tmp = g_key_file_get_integer(key_file, game->mode, "width", &tmp_err);
	if(tmp_err) {
		g_propagate_error(err, tmp_err);
		return NULL;
	}
	if(tmp < 10 || tmp > 26 || tmp % 2 == 0) {
		g_set_error(err, TILES_FILE_ERROR, TILES_FILE_ERROR_BADWIDTH, _("width '%i' for board '%s' is invalid (must be an odd number)"), tmp, game->mode);
		return NULL;
	}
	game->board_size = (guint)tmp;
	tmp = g_key_file_get_integer(key_file, game->mode, "bonus", &tmp_err);
	if(tmp_err) {
		game->bonus = 50;
		g_clear_error(&tmp_err);
	} else {
		game->bonus = tmp;
	}
	tmp = g_key_file_get_integer(key_file, game->mode, "rack", &tmp_err);
	if(tmp_err) {
		game->rack_size = 7;
		g_clear_error(&tmp_err);
	} else {
		game->rack_size = (guint)tmp;
	}
	square_t **res = g_new(square_t *, game->board_size);
	for(i=0; i<game->board_size; i++) {
		g_snprintf(key, sizeof(key), "row_%lu", i);
		gsize squares_len;
		gint *squares = g_key_file_get_integer_list(key_file, game->mode, key, &squares_len, &tmp_err);
		if(tmp_err) {
			g_propagate_error(err, tmp_err);
			free_board(res, game->board_size);
			return NULL;
		}
		if(!squares || !squares_len) {
			g_set_error(err, TILES_FILE_ERROR, TILES_FILE_ERROR_MISSINGROWS, _("not enough rows found for board '%s'"), game->mode);
			free_board(res, game->board_size);
			return NULL;
		}
		if(squares_len != game->board_size) {
			g_set_error(err, TILES_FILE_ERROR, TILES_FILE_ERROR_MISSINGCOLS, _("every row in board '%s' must have 'width' squares"), game->mode);
			free_board(res, game->board_size);
			g_free(squares);
			return NULL;
		}
		res[i] = g_new0(square_t, game->board_size);
		for(gsize j=0; j<game->board_size; j++) {
			if(squares[j] < 0 || (guint)(squares[j]) > game->square_types->len) {
				g_set_error(err, TILES_FILE_ERROR, TILES_FILE_ERROR_BADTYPE, _("invalid premium square type '%i' in '%s'"), squares[j], game->mode);
				free_board(res, game->board_size);
				g_free(squares);
				return NULL;
			}
			res[i][j].type  = (guint)squares[j];
			res[i][j].tile  = -1;
			res[i][j].blank = FALSE;
		}
		g_free(squares);
	}
	return res;
}

/*
 * Load an alphabet, in the Quackle alphabet file format.
 * See the Quackle project here: <https://github.com/quackle/quackle>
 * Return a GArray of letter_t structures.
 */
GArray *
load_alphabet(const gchar  *lang,
              const gchar  *mode,
              const gchar  *datadir,
              GError      **err)
{
	gchar *filename, *raw, **lines;
	if(!mode || !g_strcmp0(mode, "scrabble")) {
		filename = g_strconcat(lang, ALPHA_EXT, NULL);
	} else {
		filename = g_strconcat(lang, "_", mode, ALPHA_EXT, NULL);
	}
	raw = get_raw_data(datadir, ALPHA_DIR, filename, err);
	g_free(filename);
	if(!raw) {
		return NULL;
	}
	lines = g_strsplit_set(raw, "\r\n", -1);
	g_free(raw);
	if(!lines) {
		return NULL;
	}
	GArray *res = g_array_new(FALSE, TRUE, sizeof(letter_t));
	for(guint i=0; i<g_strv_length(lines); i++) {
		// Tab separated fields are: uppercase, lowercase, score, frequency
		// We ignore lines starting with #, or with fewer than 4 fields
		if(lines[i][0] != '#' && lines[i][0] != '\0') {
			gchar **fields = g_strsplit(lines[i], "\t", -1);
			guint fields_len = g_strv_length(fields);
			letter_t new_letter;
			new_letter.in_bag = 0;
			if(!g_ascii_strcasecmp(fields[0], "blank")) {
				if(fields_len == 3) {
					new_letter.score = strtol(fields[1], NULL, 0);
					new_letter.freq  = strtoul(fields[2], NULL, 0);
				} else if(fields_len >= 4) {
					new_letter.score = strtol(fields[2], NULL, 0);
					new_letter.freq  = strtoul(fields[3], NULL, 0);
				} else {
					g_strfreev(fields);
					continue;
				}
				new_letter.label = NULL;
			} else if(fields_len >= 4) {
				new_letter.label = g_strdup(fields[0]);
				new_letter.score = strtol(fields[2], NULL, 0);
				new_letter.freq  = strtoul(fields[3], NULL, 0);
			}
			g_array_append_val(res, new_letter);
			g_strfreev(fields);
		}
	}
	g_strfreev(lines);
	g_array_sort(res, cmp_letter);
	return res;
}
