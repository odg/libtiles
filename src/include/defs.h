/* Macros and definitions which are only used internally by libtiles. */

#ifndef _DEFS_H_
#define _DEFS_H_

#define G_DISABLE_DEPRECATED

#include "config.h"
#include "tiles.h"

#include <glib/gi18n-lib.h>

#define LEXICA_EXT ".list"
#define LEXICA_DIR "lexica"
#define ALPHA_EXT  ".quackle_alphabet"
#define ALPHA_DIR  "alphabets"

#define GET_LETTER(letters, i) g_array_index(letters, letter_t, (guint) (i))
#define GET_TYPE(game, i)      g_array_index(game->square_types, premium_t, (i) - 1)

#define GET_SQUARE(game, pos)   ((game)->board[(pos).row][(pos).col])
#define POS_IN_RANGE(game, pos) (((pos).row < (game)->board_size) && ((pos).col < (game)->board_size))

#define AT_START(game) ((game)->moves == 0)

#define STREQ(a, b) (g_strcmp0((a), (b)) == 0)

/** Content and type of square */
typedef struct square_t
{
	gint     tile;  ///< Index of tile in the square, or -1 if empty
	guint    type;  ///< Type of square, if it is premium
	gboolean blank; ///< Is the tile a blank?
} square_t;

typedef struct premium_t
{
	gchar   *label;
	gboolean word;
	glong    multi;
} premium_t;

/** A letter of the alphabet, and its frequency/score */
typedef struct letter_t
{
	gchar   *label;  ///< The letter's UTF-8 representation
	gulong   freq;   ///< Starting letter frequency
	gulong   in_bag; ///< Current letter frequency
	glong    score;  ///< Score value of the letter
} letter_t;

typedef struct move_t
{
	gchar     *word;   ///< Word of each move
	pos_t     *pos;    ///< Positions of each word
	glong      score;  ///< Score for each move
	guint      player; ///< Player ID who made the move
	move_type  type;   ///< Type of move
	GDateTime *time;   ///< Time of the move
} move_t;

struct game_t
{
	square_t   **board;
	move_t      *move_log;
	GHashTable  *words;
	gint       **racks, bonus;
	GArray      *letters, *square_types;
	gchar       *lang, *lang_var, *mode;
	guint        players, moves, board_size, rack_size;
	GDateTime   *start, *end;
	GRand       *rng;
};

#endif // _DEFS_H_
