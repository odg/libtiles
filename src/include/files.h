#ifndef _FILES_H_
#define _FILES_H_

#include "defs.h"

GHashTable *
load_lexicon(const gchar *lang, const gchar *lang_var, const gchar *datadir, GError **err);

square_t **
load_board(game_t *game, const gchar *datadir, GError **err);

GArray *
load_alphabet(const gchar *lang, const gchar *mode, const gchar *datadir, GError **err);

#endif // _FILES_H_
