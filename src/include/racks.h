#ifndef _RACKS_H_
#define _RACKS_H_

#include "defs.h"

gint *
new_rack(const game_t *game);

gint *
copy_rack(const game_t *game, guint player);

void
fill_rack(game_t *game, guint player);

gboolean
check_rack(const game_t *game, guint player, const gint *word, gsize word_len, gboolean allow_blank);

gboolean
find_in_rack(const game_t *game, gint *rack, gint letter);

gint
find_blank_in_rack(const game_t *game, gint *rack);

void
free_racks(gint **racks, guint players);

#endif // _RACKS_H_
