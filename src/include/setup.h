#ifndef _SETUP_H_
#define _SETUP_H_

#include "defs.h"

gint cmp_letter(gconstpointer a,
                gconstpointer b);

void free_alpha(GArray *letters);
void free_board(square_t **board, guint board_size);
void free_moves(move_t *move_log, guint moves);
void free_types(GArray *types);

#endif // _SETUP_H_
