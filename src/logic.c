#include "defs.h"
#include "racks.h"
#include "setup.h"

static inline letter_t *
get_blank(const game_t *game)
{
	// Blank, if it exists, should always be last thanks to cmp_letter
	letter_t *res = &GET_LETTER(game->letters, game->letters->len - 1);
	if(!(res->label)) {
		return res;
	}
	return NULL;
}

/* Move position 'in', by 'dist' in direction 'dir', and return the result. */
static inline pos_t
move_pos(pos_t in, dir_t dir, guint dist)
{
	pos_t out = {0};
	out.row = in.row;
	out.col = in.col;
	switch(dir) {
		case DIR_UP:    out.row -= dist; break;
		case DIR_DOWN:  out.row += dist; break;
		case DIR_LEFT:  out.col -= dist; break;
		case DIR_RIGHT: out.col += dist; break;
	}
	return out;
}

/* Return the opposite direction to 'dir'. */
static inline dir_t
opposite(dir_t dir)
{
	switch(dir) {
		case DIR_UP:    return DIR_DOWN;  break;
		case DIR_DOWN:  return DIR_UP;    break;
		case DIR_LEFT:  return DIR_RIGHT; break;
		case DIR_RIGHT: return DIR_LEFT;  break;
	}
	return -1;
}

static inline void
build_word(const game_t *game, GString *s, gboolean append, gboolean paren, gint li)
{
	const gchar *label;
	if(li < 0) {
		return;
	}
	if(!(label = tiles_get_letter(game, (guint)li))) {
		return;
	}
	if(append) {
		if(paren) {
			g_string_append(s, "(");
		}
		g_string_append(s, label);
		if(paren) {
			g_string_append(s, ")");
		}
	} else {
		if(paren) {
			g_string_prepend(s, ")");
		}
		g_string_prepend(s, label);
		if(paren) {
			g_string_prepend(s, "(");
		}
	}
}

static inline void
search_board(const game_t *game, GString *s, GString *move_desc, pos_t pos, dir_t dir, gboolean append, glong *score)
{
	gboolean blank;
	gint li;
	guint i = 0;
	do {
		li = tiles_get_square_tile(game, move_pos(pos, dir, ++i), &blank);
		if(li >= 0) {
			if(move_desc) {
				build_word(game, move_desc, append, TRUE, li);
			}
			build_word(game, s, append, FALSE, li);
			if(score) {
				if(blank) {
					letter_t *blank_letter = get_blank(game);
					if(G_LIKELY(blank_letter)) {
						*score += blank_letter->score;
					}
				} else {
					*score += GET_LETTER(game->letters, li).score;
				}
			}
		}
	} while(li >= 0);
}

static inline glong
get_tile_score(const game_t *game, pos_t cur, gint li, glong *word_multi, gboolean blank)
{
	guint type_num = GET_SQUARE(game, cur).type;
	glong score;
	if(blank) {
		letter_t *blank_letter = get_blank(game);
		if(G_LIKELY(blank_letter)) {
			score = blank_letter->score;
		} else {
			score = 0;
		}
	} else {
		score = GET_LETTER(game->letters, li).score;
	}
	if(type_num) {
		premium_t type = GET_TYPE(game, type_num);
		if(type.word) {
			if(word_multi) {
				*word_multi *= type.multi;
			}
		} else {
			score *= type.multi;
		}
	}
	return score;
}

static inline void
append_move(game_t *game, guint player, glong score, gchar *move, pos_t *start, move_type type)
{
	move_t *new_move;
	game->move_log = g_renew(move_t, game->move_log, game->moves + 1);
	new_move = &(game->move_log[game->moves]);
	new_move->player = player;
	if(type == MOVE_TYPE_NORMAL) {
		new_move->word  = move;
		new_move->pos   = start;
	} else {
		new_move->word  = NULL;
		new_move->pos   = NULL;
	}
	new_move->score = score;
	new_move->type  = type;
	new_move->time  = g_date_time_new_now_utc();

	game->moves++;
}

static inline tiles_code
change_move(game_t *game, guint player, const gint *word, gsize word_len, GError **err)
{
	if(tiles_get_remaining(game) < game->rack_size) {
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_TOOFEWTILES, _("not enough tiles remaining to do a change move"));
		return TILES_ERROR;
	}
	if(!check_rack(game, player, word, word_len, FALSE)) {
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_MISSINGTILE, _("letters contain tile which is not in the rack"));
		return TILES_ERROR;
	}
	for(gsize i=0; i<word_len; i++) {
		find_in_rack(game, game->racks[player], word[i]);
	}
	fill_rack(game, player);
	return TILES_OK;
}

static inline tiles_code
perform_move(game_t *game, guint player, const gint *word, gsize word_len, const pos_t *start)
{
	pos_t cur;
	cur.row   = start->row;
	cur.col   = start->col;
	cur.dir   = start->dir;
	for(gsize i=0; i<word_len; i++) {
		gint blank_id = -1;
		gint li       = 0;
		while(li >= 0) {
			li = tiles_get_square_tile(game, cur, NULL);
			if(li >= 0) {
				cur = move_pos(cur, start->dir, 1);
			}
		}
		li = word[i];
		if(!find_in_rack(game, game->racks[player], li)) {
			blank_id = find_blank_in_rack(game, game->racks[player]);
		}
		GET_SQUARE(game, cur).tile = li; // Place the tile
		if(blank_id >= 0) {
			GET_SQUARE(game, cur).blank = TRUE; // Flag tile as blank if necessary
		}
		cur = move_pos(cur, start->dir, 1);
	}
	fill_rack(game, player);
	for(guint i=0; i<game->rack_size; i++) {
		if(game->racks[player][i] >= 0) {
			return TILES_OK;
		}
	}
	return TILES_GAME_OVER;
}

static inline glong
validate_move(const game_t *game, guint player, const gint *word, gsize word_len, const pos_t *start, gchar **move, GError **err)
{
	g_autoptr(GString) main_word = NULL;
	GString *move_desc;
	gboolean middle_found = FALSE;
	gboolean touching     = FALSE;
	guint    middle       = (game->board_size - 1) / 2;
	glong    total_score = 0;
	glong    main_score  = 0;
	glong    main_multi  = 1;
	pos_t    cur;
	dir_t    opp_dir = opposite(start->dir);
	gint    *rack;

	if(move) {
		*move = NULL;
	}
	if(opp_dir == (dir_t)-1) {
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_DIRECTION, _("invalid direction"));
		return 0;
	}
	cur.row   = start->row;
	cur.col   = start->col;
	cur.dir   = start->dir;
	main_word = g_string_sized_new(word_len);
	move_desc = g_string_new(NULL);
	rack      = copy_rack(game, player);

	if(!AT_START(game)) {
		search_board(game, main_word, move_desc, cur, opp_dir, FALSE, &main_score);
	}
	if(main_word->len) {
		touching = TRUE;
	}
	for(gsize i=0; i<word_len; i++) {
		g_autoptr(GString) ext_word = NULL;
		glong ext_score = 0;
		gint  li        = 0;
		gint  blank_id  = -1;

		while(li >= 0) {
			li = tiles_get_square_tile(game, cur, NULL);
			if(li >= 0) {
				build_word(game, main_word, TRUE, FALSE, li);
				build_word(game, move_desc, TRUE, TRUE,  li);
				cur = move_pos(cur, start->dir, 1);
			}
			if(!POS_IN_RANGE(game, cur)) {
				g_free(rack);
				g_string_free(move_desc, TRUE);
				g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_OUTOFBOUNDS, _("tiles don't fit on the board"));
				return 0;
			}
		}
		if(cur.row == middle && cur.col == middle) {
			middle_found = TRUE;
		}
		li = word[i];
		if(!find_in_rack(game, rack, li)) {
			blank_id = find_blank_in_rack(game, rack);
			if(blank_id == -1) {
				g_free(rack);
				g_string_free(move_desc, TRUE);
				g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_MISSINGTILE, _("word contains tile which is not in the rack"));
				return 0;
			}
		}
		if(li < 0 || (guint)li >= game->letters->len) {
			g_free(rack);
			g_string_free(move_desc, TRUE);
			g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_BADLETTER, _("letter index is invalid"));
			return 0;
		}

		ext_word = g_string_new(NULL);

		build_word(game, main_word, TRUE, FALSE, li);
		build_word(game, ext_word,  TRUE, FALSE, li);
		build_word(game, move_desc, TRUE, FALSE, li);

		if(start->dir == DIR_DOWN) {
			search_board(game, ext_word, NULL, cur, DIR_LEFT,  FALSE, &ext_score);
			search_board(game, ext_word, NULL, cur, DIR_RIGHT, TRUE,  &ext_score);
		} else if(start->dir == DIR_RIGHT) {
			search_board(game, ext_word, NULL, cur, DIR_UP,   FALSE, &ext_score);
			search_board(game, ext_word, NULL, cur, DIR_DOWN, TRUE,  &ext_score);
		}
		if(ext_word->len > 1) {
			glong ext_multi;
			if(!tiles_is_word_valid(game, ext_word->str)) {
				g_free(rack);
				g_string_free(move_desc, TRUE);
				g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_BADSIDEWORD, _("word '%s' is not in the lexicon"), ext_word->str);
				return 0;
			}

			touching = TRUE;
			ext_multi = 1;
			ext_score += get_tile_score(game, cur, li, &ext_multi, blank_id >= 0);
			total_score += ext_score * ext_multi;
		}
		main_score += get_tile_score(game, cur, li, &main_multi, blank_id >= 0);
		cur = move_pos(cur, start->dir, 1);
	}
	g_free(rack);
	if(AT_START(game) && !middle_found) {
		g_string_free(move_desc, TRUE);
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_BADSTART, _("initial word must be on the middle square"));
		return 0;
	}
	search_board(game, main_word, move_desc, cur, start->dir, TRUE, &main_score);
	if(main_word->len > word_len) {
		touching = TRUE;
	}
	if(!AT_START(game) && !touching) {
		g_string_free(move_desc, TRUE);
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_NOTOUCH, _("new words must touch at least one other word on the board"));
		return 0;
	}
	if(main_word->len < 2) {
		g_string_free(move_desc, TRUE);
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_TOOSHORT, _("words must be at least two letters long"));
		return 0;
	}
	if(!tiles_is_word_valid(game, main_word->str)) {
		g_string_free(move_desc, TRUE);
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_BADMAINWORD, _("main word is not in the lexicon"));
		return 0;
	}
	total_score += main_score * main_multi;
	if(word_len == game->rack_size) {
		total_score += game->bonus;
	}
	if(G_LIKELY(move)) {
		*move = g_string_free(move_desc, FALSE);
	} else {
		g_string_free(move_desc, TRUE);
	}
	return total_score;
}

tiles_code
tiles_move_new(game_t *game, const gint *word, gsize word_len, pos_t *start, move_type type, GError **err)
{
	g_return_val_if_fail(err == NULL || *err == NULL, TILES_ERROR);
	g_return_val_if_fail(game, TILES_ERROR);

	GError     *tmp_err  = NULL;
	guint       player   = tiles_get_next_player(game);
	glong       score    = 0;
	gchar      *move_str = NULL;
	tiles_code  ret      = TILES_OK;

	if(!(game->start)) {
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_PREGAME, _("attempted move before game has started"));
		return TILES_ERROR;
	}
	if(game->end) {
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_POSTGAME, _("attempted move after game has ended"));
		return TILES_ERROR;
	}

	switch(type) {
	    case MOVE_TYPE_NORMAL:
		if(!word || !word_len) {
			g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_NOWORD, _("no word given"));
			return TILES_ERROR;
		}
		score = validate_move(game, player, word, word_len, start, &move_str, &tmp_err);
		if(tmp_err) {
			g_propagate_error(err, tmp_err);
			return TILES_ERROR;
		}
		ret = perform_move(game, player, word, word_len, start);
		break;
	    case MOVE_TYPE_CHANGE:
		ret = change_move(game, player, word, word_len, err);
		break;
	    case MOVE_TYPE_RESIGN:
		ret = TILES_GAME_OVER;
		break;
	    case MOVE_TYPE_PASS:
		break;
	    default:
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_BADMOVETYPE, _("invalid move type"));
		return TILES_ERROR;
	}
	if(ret != TILES_ERROR) {
		append_move(game, player, score, move_str, start, type);
	}
	return ret;
}

tiles_code
tiles_move_new_from_ref(game_t *game, const gint *word, gsize word_len, const gchar *ref, GError **err)
{
	pos_t pos, *posptr;
	posptr = tiles_parse_square_ref(game, ref, &pos);
	if(!posptr) {
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_BADPOSREF, _("invalid position reference"));
		return TILES_ERROR;
	}
	return tiles_move_new(game, word, word_len, posptr, MOVE_TYPE_NORMAL, err);
}

static inline gint *
parse_word_str(const game_t *game, const char *word, gsize *word_len, GError **err)
{
	gint *res;
	char **parts;
	gsize i = 0;

	parts = g_strsplit_set(word, "\t ", -1);
	res = g_new0(gint, g_strv_length(parts));
	while(parts[i]) {
		guint li;
		letter_t tmp;
		tmp.label = parts[i];
		if(!g_array_binary_search(game->letters, &tmp, cmp_letter, &li)) {
			g_free(res);
			g_strfreev(parts);
			g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_BADLETTER, _("string includes invalid letter"));
			return NULL;
		}
		g_assert(li <= G_MAXINT);
		res[i] = (gint)li;
		i++;
	}
	g_strfreev(parts);
	if(word_len) {
		*word_len = i;
	}
	return res;
}

tiles_code
tiles_move_new_from_str(game_t *game, const char *word_str, const gchar *ref, GError **err)
{
	pos_t pos, *posptr;
	gint *word;
	gsize word_len = 0;
	tiles_code ret;

	posptr = tiles_parse_square_ref(game, ref, &pos);
	if(!posptr) {
		g_set_error(err, TILES_MOVE_ERROR, TILES_MOVE_ERROR_BADPOSREF, _("invalid position reference"));
		return TILES_ERROR;
	}
	word = parse_word_str(game, word_str, &word_len, err);
	if(!word) {
		return TILES_ERROR;
	}
	ret = tiles_move_new(game, word, word_len, posptr, MOVE_TYPE_NORMAL, err);
	g_free(word);
	return ret;
}
