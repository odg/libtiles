#include "defs.h"

char *
tiles_get_move_str(const game_t *game, guint move, GError **err)
{
	move_t *m;
	char *ref, *res;
	g_return_val_if_fail(err == NULL || *err == NULL, NULL);
	if(move >= game->moves) {
		g_set_error(err, TILES_ARGS_ERROR, TILES_ARGS_ERROR_BADMOVEID, _("invalid move ID"));
		return NULL;
	}
	m = &(game->move_log[move]);
	ref = tiles_get_square_ref(game, *(m->pos));
	res = g_strdup_printf("%s %li %s", ref, m->score, m->word);
	g_free(ref);
	return res;
}

const char *
tiles_get_letter(const game_t *game, guint n)
{
	if(n < game->letters->len) {
		const char *ret = GET_LETTER(game->letters, n).label;
		if(ret) {
			return ret;
		}
		return "?";
	}
	return NULL;
}

char *
tiles_get_square_ref(const game_t *game, pos_t pos)
{
	if(POS_IN_RANGE(game, pos)) {
		char *res;
		const char *letter = tiles_get_letter(game, pos.row);
		res = g_strdup_printf("%s%u", letter, pos.col);
		return res;
	}
	return NULL;
}

const char *
tiles_get_square_label(const game_t *game, pos_t pos)
{
	if(POS_IN_RANGE(game, pos)) {
		guint type_num = GET_SQUARE(game, pos).type;
		if(type_num) {
			premium_t type = GET_TYPE(game, type_num);
			return type.label;
		}
	}
	return NULL;
}

const char *
tiles_get_square_label_long(const game_t *game, pos_t pos)
{
	if(POS_IN_RANGE(game, pos)) {
		guint type_num = GET_SQUARE(game, pos).type;
		if(type_num) {
			premium_t type = GET_TYPE(game, type_num);
			if(type.word) {
				if(type.multi == 2) {
					return _("Double Word Score");
				} else if(type.multi == 3) {
					return _("Triple Word Score");
				} else if(type.multi == 4) {
					return _("Quadruple Word Score");
				}
			} else {
				if(type.multi == 2) {
					return _("Double Letter Score");
				} else if(type.multi == 3) {
					return _("Triple Letter Score");
				} else if(type.multi == 4) {
					return _("Quadruple Letter Score");
				}
			}
		}
	}
	return NULL;
}

gint
tiles_get_square_tile(const game_t *game, pos_t pos, gboolean *blank)
{
	if(POS_IN_RANGE(game, pos)) {
		if(blank) {
			*blank = GET_SQUARE(game, pos).blank;
		}
		return GET_SQUARE(game, pos).tile;
	}
	return -2;
}

const char *
tiles_get_square_tile_label(const game_t *game, pos_t pos, gboolean *blank)
{
	gint index = tiles_get_square_tile(game, pos, blank);
	if(index >= 0) {
		return tiles_get_letter(game, (guint)index);
	}
	return NULL;
}
