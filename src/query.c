/* Public functions for querying the game state.
   These should all be NULL checked and bounds checked where necessary.
 */

#include "defs.h"

guint
tiles_get_num_moves(const game_t *game)
{
	return game->moves;
}

guint
tiles_get_num_players(const game_t *game)
{
	return game->players;
}

guint
tiles_get_next_player(const game_t *game)
{
	if(game->moves) {
		return (game->move_log[game->moves - 1].player + 1) % game->players;
	}
	return 0;
}

guint
tiles_get_board_size(const game_t *game)
{
	return game->board_size;
}

guint
tiles_get_rack_size(const game_t *game)
{
	return game->rack_size;
}

int
tiles_get_bonus(const game_t *game)
{
	return game->bonus;
}

gulong
tiles_get_remaining(const game_t *game)
{
	gulong res = 0;
	for(guint i=0; i<game->letters->len; i++) {
		res += GET_LETTER(game->letters, i).in_bag;
	}
	return res;
}

const char *
tiles_get_lang(const game_t *game)
{
	return game->lang;
}

const char *
tiles_get_mode(const game_t *game)
{
	return game->mode;
}

char **
tiles_get_square_label_list(const game_t *game)
{
	g_return_val_if_fail(game, 0);
	char **res = g_malloc_n(game->square_types->len + 1, sizeof(char *));
	for(guint i=1; i<=game->square_types->len; i++) {
		res[i - 1] = g_strdup(GET_TYPE(game, i).label);
	}
	res[game->square_types->len] = NULL;
	return res;
}

long
tiles_get_letter_score(const game_t *game, guint letter, GError **err)
{
	g_return_val_if_fail(err == NULL || *err == NULL, 0);
	g_return_val_if_fail(game, 0);
	if(letter >= game->letters->len) {
		g_set_error(err, TILES_ARGS_ERROR, TILES_ARGS_ERROR_BADMOVEID, _("invalid move ID"));
		return 0;
	}
	return GET_LETTER(game->letters, letter).score;
}

long
tiles_get_move_score(const game_t *game, guint move, GError **err)
{
	g_return_val_if_fail(err == NULL || *err == NULL, 0);
	g_return_val_if_fail(game, 0);
	if(move >= game->moves) {
		g_set_error(err, TILES_ARGS_ERROR, TILES_ARGS_ERROR_BADMOVEID, _("invalid move ID"));
		return 0;
	}
	return game->move_log[move].score;
}

const char *
tiles_get_move_word(const game_t *game, guint move, GError **err)
{
	g_return_val_if_fail(err == NULL || *err == NULL, NULL);
	g_return_val_if_fail(game, NULL);
	if(move >= game->moves) {
		g_set_error(err, TILES_ARGS_ERROR, TILES_ARGS_ERROR_BADMOVEID, _("invalid move ID"));
		return NULL;
	}
	return game->move_log[move].word;
}

const gint *
tiles_get_player_rack(const game_t *game, guint player, GError **err)
{
	g_return_val_if_fail(err == NULL || *err == NULL, NULL);
	g_return_val_if_fail(game, NULL);
	if(player >= game->players) {
		g_set_error(err, TILES_ARGS_ERROR, TILES_ARGS_ERROR_BADPLAYERID, _("invalid player ID"));
		return NULL;
	}
	return game->racks[player];
}

long
tiles_get_player_score(const game_t *game, guint player, GError **err)
{
	g_return_val_if_fail(err == NULL || *err == NULL, 0);
	g_return_val_if_fail(game, 0);
	if(player >= game->players) {
		g_set_error(err, TILES_ARGS_ERROR, TILES_ARGS_ERROR_BADPLAYERID, _("invalid player ID"));
                return 0;
	}
	long res = 0;
	for(guint i=0; i<game->moves; i++) {
		if(game->move_log[i].player == player) {
			res += game->move_log[i].score;
		}
	}
	return res;
}

gboolean
tiles_is_word_valid(const game_t *game, const char *word)
{
	return g_hash_table_lookup(game->words, word) ? TRUE : FALSE;
}

pos_t *
tiles_parse_square_ref(const game_t *game, const char *ref, pos_t *pos)
{
	glong len;
	gunichar c;
	char *ptr = (char *)ref;
	guint i;
	int tmp;
	if(!ref || !pos) {
		return NULL;
	}
	len = g_utf8_strlen(ref, -1);
	if(len != 3) {
		return NULL;
	}
	c = g_utf8_get_char_validated(ptr, -1);
	if(c == (gunichar)-1 || c == (gunichar)-2) {
		return NULL;
	}
	for(i=0; i<game->letters->len; i++) {
		if(g_utf8_get_char(tiles_get_letter(game, i)) == c) {
			pos->row = i;
			break;
		}
	}
	if(i == game->letters->len) {
		return NULL;
	}
	ptr = g_utf8_next_char(ptr);
	c = g_utf8_get_char_validated(ptr, -1);
	if(c == (gunichar)-1 || c == (gunichar)-2) {
		return NULL;
	}
	tmp = g_unichar_digit_value(c);
	if(tmp < 1 || (guint)tmp > game->letters->len) {
		return NULL;
	}
	pos->col = (guint)tmp - 1;
	ptr = g_utf8_next_char(ptr);
	c = g_utf8_get_char_validated(ptr, -1);
	switch(c) {
		case DIR_UP:    pos->dir = DIR_UP;    break;
		case DIR_DOWN:  pos->dir = DIR_DOWN;  break;
		case DIR_LEFT:  pos->dir = DIR_LEFT;  break;
		case DIR_RIGHT: pos->dir = DIR_RIGHT; break;
		default: return NULL;
	}
	return pos;
}

gsize
tiles_check_winners(const game_t *game, glong **scores_in, guint **winners, GError **err)
{
	g_return_val_if_fail(err == NULL || *err == NULL, 0);
	g_return_val_if_fail(game, 0);

	glong    max_score   = 0;
	gboolean resigned    = FALSE;
	guint    resigner    = 0;
	gsize    num_winners = 0;
	glong   *scores;

	if(game->moves == 0 || !tiles_get_start_time(game)) {
		return 0;
	}

	scores = g_new(glong, game->players);
	if(game->move_log[game->moves - 1].type == MOVE_TYPE_RESIGN) {
		resigned = TRUE;
		resigner = game->move_log[game->moves - 1].player;
	}
	for(guint i=0; i<game->players; i++) {
		scores[i] = tiles_get_player_score(game, i, err);
		if(!resigned || resigner != i) {
			max_score = MAX(scores[i], max_score);
		}
	}
	if(winners) {
		*winners = NULL;
	}
	for(guint i=0; i<game->players; i++) {
		if(scores[i] == max_score && (!resigned || resigner != i)) {
			num_winners++;
			if(winners) {
				*winners = g_renew(guint, *winners, num_winners);
				(*winners)[num_winners - 1] = i;
			}
		}
	}
	if(scores_in) {
		*scores_in = scores;
	} else {
		g_free(scores);
	}
	return num_winners;
}
