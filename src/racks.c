#include "racks.h"

gint *
new_rack(const game_t *game)
{
	gint *res = g_malloc_n(game->rack_size, sizeof(**(game->racks)));
	for(guint i=0; i<game->rack_size; i++) {
		res[i] = -1;
	}
	return res;
}

gint *
copy_rack(const game_t *game, guint player)
{
	gint *out = new_rack(game);
	for(guint i=0; i<game->rack_size; i++) {
		out[i] = game->racks[player][i];
	}
	return out;
}

void
fill_rack(game_t *game, guint player)
{
	gulong total = tiles_get_remaining(game);
	for(guint i=0; i<game->rack_size; i++) {
		if(total == 0) {
			break;
		}
		if(game->racks[player][i] >= 0) {
			continue;
		}
		gulong rand = (gulong)g_rand_int_range(game->rng, 0, (int)total);
		for(guint j=0; j<game->letters->len; j++) {
			if(rand <= GET_LETTER(game->letters, j).in_bag) {
				g_assert(j <= G_MAXINT);
				game->racks[player][i] = (gint)j;
				GET_LETTER(game->letters, j).in_bag--;
				total--;
				break;
			}
			rand -= GET_LETTER(game->letters, j).in_bag;
		}
	}
}

gboolean
check_rack(const game_t *game, guint player, const gint *word, gsize word_len, gboolean allow_blank)
{
	gboolean ret = TRUE;
	gint *rack = copy_rack(game, player);
	for(gsize i=0; i<word_len; i++) {
		if(word[i] < 0) {
			ret = FALSE;
			break;
		}
		if(!find_in_rack(game, rack, word[i])) {
			if(allow_blank) {
				if(!find_blank_in_rack(game, rack)) {
					ret = FALSE;
					break;
				}
			} else {
				ret = FALSE;
				break;
			}
		}
	}
	g_free(rack);
	return ret;
}

gboolean
find_in_rack(const game_t *game, gint *rack, gint letter)
{
	if(letter < 0) {
		return FALSE;
	}
	for(guint i=0; i<game->rack_size; i++) {
		if(rack[i] == letter) {
			rack[i] = -1;
			return TRUE;
		}
	}
	return FALSE;
}

gint
find_blank_in_rack(const game_t *game, gint *rack)
{
	for(guint i=0; i<game->rack_size; i++) {
		if(rack[i] >= 0 && !tiles_get_letter(game, (guint)(rack[i]))) {
			gint blank = rack[i];
			rack[i] = -1;
			return blank;
		}
	}
	return -1;
}

void
free_racks(gint **racks, guint players)
{
	if(racks) {
		for(guint i=0; i<players; i++) {
			g_free(racks[i]);
		}
		g_free(racks);
	}
}
