#include "files.h"
#include "racks.h"
#include "setup.h"

G_DEFINE_QUARK(tiles-move-error-quark, tiles_move_error)
G_DEFINE_QUARK(tiles-file-error-quark, tiles_file_error)
G_DEFINE_QUARK(tiles-args-error-quark, tiles_args_error)

/** Create the initial game structure that gets passed everywhere */
game_t *
tiles_game_new(const char *lang, const char *mode, const char *datadir, GError **err)
{
	game_t *game;

	g_return_val_if_fail(err == NULL || *err == NULL, NULL);
	if(!lang) {
		g_set_error(err, TILES_ARGS_ERROR, TILES_ARGS_ERROR_NOLANG, _("no language given when creating new game"));
		return NULL;
	}
	if(!mode) {
		g_set_error(err, TILES_ARGS_ERROR, TILES_ARGS_ERROR_NOMODE, _("no mode given when creating new game"));
		return NULL;
	}

	bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");

	game           = g_new0(game_t, 1);
	game->lang     = g_strdup(lang);
	game->lang_var = g_strdup("british");
	game->mode     = g_strdup(mode);
	game->bonus    = 50;
	game->rack_size = 7;

	// Load data from files
	game->board   = load_board(game, datadir, err);
	if(!(game->board)) {
		tiles_game_free(game);
		return NULL;
	}
	game->words   = load_lexicon(game->lang,
	                             game->lang_var,
	                             datadir,
                                     err);
	if(!(game->words)) {
		tiles_game_free(game);
		return NULL;
	}
	game->letters = load_alphabet(game->lang,
	                              game->mode,
	                              datadir,
                                      err);
	if(!(game->letters)) {
		tiles_game_free(game);
		return NULL;
	}
	return game;
}

gint
cmp_letter(gconstpointer a,
           gconstpointer b)
{
	const gchar *letter_a = ((const letter_t *)a)->label;
	const gchar *letter_b = ((const letter_t *)b)->label;
	if(!letter_a) {
		if(!letter_b) {
			return 0;
		}
		return 1;
	}
	if(!letter_b) {
		return -1;
	}
	return g_strcmp0(letter_a, letter_b);
}

static inline void
empty_board(square_t **board, guint board_size)
{
	if(board) {
		for(guint i=0; i<board_size; i++) {
			for(guint j=0; j<board_size; j++) {
				board[i][j].tile = -1;
			}
		}
	}
}

static inline void
refill_bag(GArray *letters)
{
	if(letters) {
		for(guint i=0; i<letters->len; i++) {
			GET_LETTER(letters, i).in_bag = GET_LETTER(letters, i).freq;
		}
	}
}

void
free_alpha(GArray *letters)
{
	if(letters) {
		for(guint i=0; i<letters->len; i++) {
			g_free(g_array_index(letters, letter_t, i).label);
		}
		g_array_free(letters, TRUE);
	}
}

void
free_board(square_t **board, guint board_size)
{
	if(board) {
		for(guint i=0; i<board_size; i++) {
			g_free(board[i]);
		}
		g_free(board);
	}
}

void
free_moves(move_t *move_log, guint moves)
{
	if(move_log) {
		for(guint i=0; i<moves; i++) {
			g_free(move_log[i].word);
			if(move_log[i].time) {
				g_date_time_unref(move_log[i].time);
			}
		}
		g_free(move_log);
	}
}

void
free_types(GArray *types)
{
	if(types) {
		for(guint i=0; i<types->len; i++) {
			g_free(g_array_index(types, premium_t, i).label);
		}
		g_array_free(types, TRUE);
	}
}

tiles_code
tiles_game_start(game_t *game, guint players, GError **err)
{
	g_return_val_if_fail(err == NULL || *err == NULL, TILES_ERROR);
	if(!game) {
		return TILES_ERROR;
	}
	if(players < 2) {
		g_set_error(err, TILES_ARGS_ERROR, TILES_ARGS_ERROR_LOWPLAYERS, _("invalid number of players (must be at least 2)"));
		return TILES_ERROR;
	}
	if(game->start && !(game->end)) {
		g_set_error(err, TILES_ARGS_ERROR, TILES_ARGS_ERROR_INPROGRESS, _("tried to start a game which is in progress"));
		return TILES_ERROR;
	}
	game->players = players;

	// Clear the old game
	if(game->rng) {
		g_rand_free(game->rng);
	}
	free_racks(game->racks, game->players);
	free_moves(game->move_log, game->moves);
	game->move_log = NULL;
	game->moves = 0;
	game->end = 0;
	empty_board(game->board, game->board_size);
	refill_bag(game->letters);

	// Start the clock and seed the RNG
	game->start = g_date_time_new_now_utc();
	game->rng = g_rand_new_with_seed((guint32)g_date_time_to_unix(game->start));

	// Allocate and fill the racks initially
	game->racks = g_malloc_n(game->players, sizeof(*(game->racks)));
	for(guint player=0; player<game->players; player++) {
		game->racks[player] = new_rack(game);
		fill_rack(game, player);
	}
	return TILES_OK;
}

void
tiles_game_free(game_t *game)
{
	if(!game) {
		return;
	}

	free_alpha(game->letters);
	free_board(game->board,    game->board_size);
	free_racks(game->racks,    game->players);
	free_moves(game->move_log, game->moves);
	free_types(game->square_types);

	if(game->words) {
		g_hash_table_destroy(game->words);
	}
	if(game->start) {
		g_date_time_unref(game->start);
	}
	if(game->end) {
		g_date_time_unref(game->end);
	}
	if(game->rng) {
		g_rand_free(game->rng);
	}
	g_free(game->lang);
	g_free(game->lang_var);
	g_free(game->mode);

	g_free(game);
}
