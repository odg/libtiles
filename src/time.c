#include "defs.h"

#include <math.h>

gint64
tiles_get_start_time(const game_t *game)
{
	if(!(game->start)) {
		return 0;
	}
	return g_date_time_to_unix(game->start);
}

gint64
tiles_get_end_time(const game_t *game)
{
	if(!(game->end)) {
		return 0;
	}
	return g_date_time_to_unix(game->end);
}

GTimeSpan
tiles_get_duration(const game_t *game)
{
	GTimeSpan res;
	if(game->end) {
		res = g_date_time_difference(game->end, game->start);
	} else {
		GDateTime *now = g_date_time_new_now_utc();
		res = g_date_time_difference(now, game->start);
		g_date_time_unref(now);
	}
	return res;
}

char *
tiles_get_duration_str_long(const game_t *game, int dp)
{
	char *res = NULL;
	GTimeSpan dur = tiles_get_duration(game);
	double sec = fmod((double)dur / G_TIME_SPAN_SECOND, 60);
	if(dur < G_TIME_SPAN_MINUTE) {
		res = g_strdup_printf(_("%.*f seconds"), dp, sec);
	} else {
		long min = dur / G_TIME_SPAN_MINUTE;
		if(dur < G_TIME_SPAN_HOUR) {
			res = g_strdup_printf(_("%li minutes, %.*f seconds"), min, dp, sec);
		} else {
			long hr = dur / G_TIME_SPAN_HOUR;
			res = g_strdup_printf(_("%li hours, %li minutes, %.*f seconds"), hr, min % 60, dp, sec);
		}
	}
	return res;
}

char *
tiles_get_duration_str(const game_t *game, int dp)
{
	char *res = NULL;
	GTimeSpan dur = tiles_get_duration(game);
	double sec = fmod((double)dur / G_TIME_SPAN_SECOND, 60);
	long min   = dur / G_TIME_SPAN_MINUTE;
	long hr    = dur / G_TIME_SPAN_HOUR;
	res = g_strdup_printf("%02li:%02li:%02.*f", hr, min % 60, dp, sec);
	return res;
}
