#include "draw.h"

#include <stdio.h>

static inline void
print_line(GString *str, const char *start, const char *middle, const char *end, guint width)
{
	g_string_append(str, start);
	for(guint col=1; col<width-1; col++) {
		g_string_append(str, middle);
	}
	g_string_append_printf(str, "%s\n", end);
}

static inline void
print_letters(const game_t *game, GString *str, int spacing)
{
	g_string_append_printf(str, "%*c", 6 - spacing, ' ');
	for(guint col=0; col<tiles_get_board_size(game); col++) {
		g_string_append_printf(str, "%*s", spacing + 1, tiles_get_letter(game, col));
	}
}

static inline void
print_square(const game_t *game, GString *str, guint row, guint col)
{
	pos_t pos;
	const char *tile;
	gboolean blank;
	pos.row = row;
	pos.col = col;
	tile = tiles_get_square_tile_label(game, pos, &blank);
	if(tile) {
		g_string_append_printf(str, "%s ", tile);
	} else {
		const char *type;
		type = tiles_get_square_label(game, pos);
		if(!type || strlen(type) < 2) {
			g_string_append(str, "  ");
		} else if(type[1] == 'W') {
			if(type[0] == 'D') {
				g_string_append(str, "● ");
			} else if(type[0] == 'T') {
				g_string_append(str, "▲ ");
			} else if(type[0] == 'Q') {
				g_string_append(str, "■ ");
			} else {
				g_string_append(str, "⭐");
			}
		} else {
			if(type[0] == 'D') {
				g_string_append(str, "○ ");
			} else if(type[0] == 'T') {
				g_string_append(str, "△ ");
			} else if(type[0] == 'Q') {
				g_string_append(str, "□ ");
			} else {
				g_string_append(str, "✩ ");
			}
		}
	}
}

static inline void
print_data(const game_t *game, GString *str, guint row, gboolean fancy)
{
	g_string_append_printf(str, " %2i %s ", row + 1, fancy ? "║" : "|");
	print_square(game, str, row, 0);
	for(guint col=1; col<tiles_get_board_size(game); col++) {
		if(fancy) {
			g_string_append(str, "│ ");
		}
		print_square(game, str, row, col);
	}
	if(fancy) {
		g_string_append_printf(str, "║ %i", row + 1);
	}
	g_string_append(str, "\n");
}

static inline GString *
fancy_grid(const game_t *game)
{
	guint board_size = tiles_get_board_size(game);
	GString *str = g_string_new(NULL);
	print_letters(game, str, 3);
	g_string_append(str, "\n");
	for(guint row=0; row<board_size; row++) {
		if(row == 0) {
			print_line(str, "    ╔═══", "╤═══", "╤═══╗", board_size);
		}
		print_data(game, str, row, TRUE);
		if(row == board_size - 1) {
			print_line(str, "    ╚═══", "╧═══", "╧═══╝", board_size);
		} else {
			print_line(str, "    ╟───", "┼───", "┼───╢", board_size);
		}
	}
	print_letters(game, str, 3);
	return str;
}

static inline GString *
plain_grid(const game_t *game)
{
	guint board_size = tiles_get_board_size(game);
	GString *str = g_string_new(NULL);
	for(guint row=0; row<board_size; row++) {
		print_data(game, str, row, FALSE);
	}
	print_line(str, "    |", "__", "____", board_size);
	print_letters(game, str, 1);
	return str;
}

char *
tiles_get_board_str(const game_t *game, guint *x, guint *y, gboolean fancy)
{
	guint board_size = tiles_get_board_size(game);
	GString *res;
	if(fancy) {
		res = fancy_grid(game);
		if(x) {
			*x = board_size * 4 + 5;
		}
		if(y) {
			*y = board_size * 2 + 3;
		}
	} else {
		res = plain_grid(game);
		if(x) {
			*x = board_size * 2 + 5;
		}
		if(y) {
			*y = board_size + 2;
		}
	}
	return g_string_free(res, FALSE);
}

void
print_board(const game_t *game, gboolean fancy)
{
	char *grid;
	grid = tiles_get_board_str(game, NULL, NULL, fancy);
	puts(grid);
	g_free(grid);
}

void
print_rack(const game_t *game, guint player)
{
	const gint *rack = tiles_get_player_rack(game, player, NULL);
	gsize rack_size = tiles_get_rack_size(game);
	for(gsize i = 0; i < rack_size; i++) {
		if(rack[i] < 0) {
			continue;
		}
		printf("%s", tiles_get_letter(game, (guint)(rack[i])));
		if(i < rack_size - 1) {
			printf(",");
		}
	}
	printf("\n");
}
