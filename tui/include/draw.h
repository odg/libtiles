#ifndef _DRAW_H_
#define _DRAW_H_

#include <tiles.h>

/**
 * Generate a string representing the board. Should be freed after use.
 * If fancy is TRUE, a fancier board is generated, using box-drawing characters.
 */
TILES_API char *
tiles_get_board_str(const game_t *game,
                    guint        *x,
                    guint        *y,
                    gboolean      fancy)
G_GNUC_WARN_UNUSED_RESULT;

void
print_board(const game_t *game, gboolean fancy);

void
print_rack(const game_t *game, guint player);

#endif
