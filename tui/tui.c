#include "config.h"
#include "draw.h"

#include <glib/gi18n.h>
#include <readline/history.h>

#include <locale.h>
#include <stdio.h>

static inline void
control_loop(game_t *game, GError **err)
{
	tiles_code res = TILES_OK;
	while(res != TILES_GAME_OVER) {
		gchar line[1024], *t;
		gchar **parts = NULL;
		gsize len;
		guint next_player = tiles_get_next_player(game);
		if(err && *err) {
			g_print("Error: %s.\n", (*err)->message);
			g_clear_error(err);
		}
		if(res != TILES_ERROR) {
			print_board(game, TRUE);
			printf("Player %u's rack:\n", next_player + 1);
			print_rack(game, next_player);
		}
		res = TILES_OK;
		printf("Player %u > ", next_player + 1);
		t = fgets(line, sizeof(line) - 1, stdin);
		if(t && *t) {
			len = strlen(t);
			if (t[len - 1] == '\n') {
				t[len - 1] = '\0';
			}
		}
		if(!t || !(line[0])) {
			res = TILES_ERROR;
			continue;
		} else {
			char *expansion;
			int result;
			result = history_expand(line, &expansion);
			if(result) {
				fprintf(stderr, "%s\n", expansion);
			}
			if(result < 0 || result == 2) {
				free(expansion);
				continue;
			}
			add_history(expansion);
			strncpy(line, expansion, sizeof(line) - 1);
			free(expansion);
		}
		if(!g_ascii_strncasecmp(line, "quit", 4) || !g_ascii_strncasecmp(line, "exit", 4)) {
			break;
		}
		parts = g_strsplit(line, " ", 2);
		if(!g_ascii_strncasecmp(line, "pass", 4)) {
			res = TILES_PASS(game);
		} else if(!g_ascii_strncasecmp(line, "resign", 6)) {
			res = TILES_RESIGN(game);
		} else if(!g_ascii_strncasecmp(line, "change", 6)) {
			if(g_strv_length(parts) < 2) {
				res = TILES_ERROR;
			} else {
//				res = TILES_CHANGE(game, parts[1], );
			}
		} else {
			res = tiles_move_new_from_str(game, "A P P L E", "H8⬇", err);
		}
		g_strfreev(parts);
	}
	if(res == TILES_GAME_OVER) {
		glong *scores;
		guint *winners;
		gsize num_winners = tiles_check_winners(game, &scores, &winners, err);
		g_print(_("Game over. "));
		if(num_winners == 1) {
			g_print(_("Player %u is the winner!\n"), winners[0] + 1);
		} else {
			g_print(_("The game was a tie.\n"));
		}
	}
}

static inline game_t *
parse_opts(int argc, char **argv, GError **err)
{
	tiles_code res;
	game_t *game = NULL;
	GOptionContext *context;

	// The option variables
	gint   players = 0;
	gchar *mode    = NULL,
              *lang    = NULL,
              *datadir = NULL;

	// List possible command line options
	const GOptionEntry entries[] =
	{
	  { "players",  'p', 0, G_OPTION_ARG_INT,      &players, "Number of players", "N"    },
	  { "mode",     'm', 0, G_OPTION_ARG_STRING,   &mode,    "Game mode",         "MODE" },
	  { "language", 'l', 0, G_OPTION_ARG_STRING,   &lang,    "Game language",     "LANG" },
	  { "datadir",  'd', 0, G_OPTION_ARG_FILENAME, &datadir, "Data directory",    "DIR"  },
	  { NULL }
	};

	// Pass the command line options given
	context = g_option_context_new(NULL);
	g_option_context_add_main_entries(context, entries, GETTEXT_PACKAGE);
	g_option_context_set_summary(context,     "A basic text interface for libtiles. Play crossword games in a terminal.");
	g_option_context_set_description(context, "Send bug reports to: Oliver Galvin <odg@riseup.net>.");
	if(!g_option_context_parse(context, &argc, &argv, err)) {
		return NULL;
	}

	game = tiles_game_new(lang    ? lang    : "english",
	                      mode    ? mode    : "scrabble",
	                      datadir ? datadir : NULL,
	                      err);
	g_free(lang);
	g_free(mode);
	g_free(datadir);
	g_option_context_free(context);

	if(!game) {
		return NULL;
	}

	res = tiles_game_start(game, players >= 2 ? (guint)players : 2, err);
	if(res < 0) {
		tiles_free(game);
		return NULL;
	}

	return game;
}

int
main(int argc, char **argv)
{
	setlocale(LC_ALL, "");
	bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);

	g_autoptr(GError) err  = NULL;
	g_autoptr(game_t) game = parse_opts(argc, argv, &err);

	if(err) {
		g_print(_("Error: %s.\n"), err->message);
		return EXIT_FAILURE;
	}

	using_history();

	control_loop(game, &err);

	if(err) {
		g_print(_("Error: %s.\n"), err->message);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
